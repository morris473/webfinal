﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class oilsearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.Visible = false;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataSet data = new DataSet();
            oil.STNWebService o = new oil.STNWebService();
            data = o.getCityStation(TextBox1.Text.ToString(), TextBox2.Text.ToString(), TextBox3.Text.ToString());
            GridView1.Visible = true;
            GridView1.DataSource = data;
            GridView1.DataBind();
            TextBox1.Text = "";
            TextBox2.Text = "";
        }
    }
}