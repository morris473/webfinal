﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="home.aspx.cs" Inherits="WebApplication1.home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <h2>網站導覽</h2>
    <p/>
    <ul>
<li style="font-size:large">航班資訊查詢:</li>使用者輸入出發地、抵達地、出發日期來查詢航班資訊<p />
<li style="font-size:large">天氣預報:</li>使用者輸入城市名稱來獲得該城市當天以及未來2天的天氣情況<p/>
<li style="font-size:large">zip code查詢地址:</li>使用者輸入郵政區碼來查詢當地的資訊<p/>
<li style="font-size:large">加油站查詢:</li>使用者輸入縣市別、鄉鎮區、加油站類別來查詢加油站的相關資訊<p/>
</ul>
</asp:Content>
