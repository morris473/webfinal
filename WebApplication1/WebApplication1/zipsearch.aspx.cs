﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class zipsearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.Visible = false;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataSet data = new DataSet();
            zip.ChinaZipSearchWebService z = new zip.ChinaZipSearchWebService();
            data = z.getAddressByZipCode(TextBox1.Text.ToString(), "");
            if (data.Tables[0].Rows[0]["CITY"].ToString() == "数据没有发现")
            {
                GridView1.Visible = false;
                Label1.Text = "查無資料";
                TextBox1.Text = "";

            }
            else
            {
                GridView1.Visible = true;
                GridView1.DataSource = data;
                GridView1.DataBind();
                TextBox1.Text = "";
                Label1.Text = "";
            }
        }
    }
}