﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="oilsearch.aspx.cs" Inherits="WebApplication1.oilsearch" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <label> <h2 style="color:blueviolet">加油站查詢</h2></label>
      <br />
     <table align="center" style="border: 1px dashed #C0C0C0" cellpadding="3" width="60%">
        <tr>
        <td>          
           <label style="color:blueviolet">(未輸入表示全部縣市):&nbsp&nbsp</label><asp:TextBox ID="TextBox1" runat="server" ReadOnly="False" Height="21px" 
           Width="130px"></asp:TextBox><br />
           <label style="color:blueviolet">鄉鎮區(未輸入表示該縣市全部鄉鎮區):&nbsp&nbsp</label><asp:TextBox ID="TextBox2" runat="server" ReadOnly="False" Height="21px" 
           Width="130px"></asp:TextBox><br />
           <label style="color:blueviolet">類別(未輸入表示全部類別):&nbsp&nbsp</label><asp:TextBox ID="TextBox3" runat="server" ReadOnly="False" Height="21px" 
           Width="130px"></asp:TextBox><p />

            <asp:Button ID="Button1" runat="server" Text="送出" onclick="Button1_Click" />
         &nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp &nbsp &nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp
        </td></tr>
        </table><p/>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        align="center" BackColor="white" BorderColor="white" CellPadding="4" 
        HorizontalAlign="Center">
        <Columns>
            <asp:BoundField DataField="站代號" HeaderText="站代號" />
            <asp:BoundField DataField="類別" HeaderText="類別" />
            <asp:BoundField DataField="站名" HeaderText="站名" />
            <asp:BoundField DataField="縣市" HeaderText="縣市" />
            <asp:BoundField DataField="鄉鎮區" HeaderText="鄉鎮區" />
            <asp:BoundField DataField="地址" HeaderText="地址" />
            <asp:BoundField DataField="電話" HeaderText="電話" />
            <asp:BoundField DataField="服務中心" HeaderText="服務中心" />        
            <asp:BoundField DataField="營業中" HeaderText="營業中" />  
            <asp:BoundField DataField="無鉛92" HeaderText="無鉛92" />  
            <asp:BoundField DataField="無鉛95" HeaderText="無鉛95" />  
            <asp:BoundField DataField="無鉛98" HeaderText="無鉛98" /> 
            <asp:BoundField DataField="酒精汽油" HeaderText="酒精汽油" />
            <asp:BoundField DataField="超柴" HeaderText="超柴" />
            <asp:BoundField DataField="會員卡" HeaderText="會員卡" />
            <asp:BoundField DataField="刷卡自助" HeaderText="刷卡自助" />
            <asp:BoundField DataField="自助柴油站" HeaderText="自助柴油站" />
            <asp:BoundField DataField="經度" HeaderText="經度" />
            <asp:BoundField DataField="緯度" HeaderText="緯度" />
            <asp:BoundField DataField="營業時間" HeaderText="營業時間" /> 

        </Columns>
        <HeaderStyle BackColor="#00CCFF" Font-Size="11pt" ForeColor="#000000" />
        <RowStyle HorizontalAlign="Center" />
    </asp:GridView><br/><br/>
</asp:Content>
