﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace WebApplication1
{
    public partial class airsearch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            DataSet data = new DataSet();
            air.DomesticAirline air = new air.DomesticAirline();
            data = air.getDomesticAirlinesTime(TextBox1.Text.ToString(), TextBox2.Text.ToString(), TextBox3.Text.ToString(), "");

            if (data.Tables[0].Rows[0]["Company"].ToString() == "日期错误")
            {
                GridView1.Visible = false;
                Label1.Text = "日期錯誤";
            }
            else if (data.Tables[0].Rows[0]["Company"].ToString() == "日期格式错误")
            {
                GridView1.Visible = false;
                Label1.Text = "日期格式錯誤";
            }
            else if (data.Tables[0].Rows[0]["Company"].ToString() == "没有航班")
            {
                GridView1.Visible = false;
                Label1.Text = "沒有航班";
            }
            else
            {
                GridView1.Visible = true;
                GridView1.DataSource = data;
                GridView1.DataBind();
                Label1.Text = "";
                TextBox1.Text = "";
                TextBox2.Text = "";
                TextBox3.Text = "";

            }
        }
    }
}